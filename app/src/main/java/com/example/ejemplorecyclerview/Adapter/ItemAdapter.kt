package com.example.ejemplorecyclerview.Adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.ejemplorecyclerview.R
import com.example.ejemplorecyclerview.Model.Affirmation

class ItemAdapter(
    var context: Context,
    var horasydireccion: List<Affirmation>
    ):RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val textView1: TextView = view.findViewById(R.id.horario)
        val textView2: TextView= view.findViewById(R.id.direccion)
        val textView: TextView= view.findViewById(R.id.textView)
        val boton1: TextView= view.findViewById(R.id.vermas)
        val boton2: TextView= view.findViewById(R.id.ir)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        var adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.listaitems, parent,false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item= horasydireccion[position]
        holder.textView1.text= context.resources.getString(item.strinbResource)
        holder.textView2.text= context.resources.getString(item.direccion)
        holder.textView.text= context.resources.getString(item.Lugar)
        holder.boton2.setOnClickListener {
            val intentUbi = Uri.parse("geo:0,0?q=-25.285242715957214, -57.57695957605754")
            // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
            val mapIntent = Intent(Intent.ACTION_VIEW, intentUbi)
            mapIntent.setPackage("com.google.android.apps.maps")
            context.startActivity(mapIntent)
        }
    }

     override fun getItemCount(): Int {
        return horasydireccion.size
    }
}