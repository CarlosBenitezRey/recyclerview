package com.example.ejemplorecyclerview.Datasource

import com.example.ejemplorecyclerview.R
import com.example.ejemplorecyclerview.Model.Affirmation

class Datasource() {
    fun loaddatas(): List<Affirmation>{
        return listOf<Affirmation>(
            Affirmation(R.string.hora1,R.string.direccion1, R.string.lugar1),
            Affirmation(R.string.hora2, R.string.direccion2, R.string.lugar2)
        )
    }
}